from tkinter import *
from tkinter import Tk, ttk
from turtle import Turtle, Screen

elsa = Turtle()
window = Screen()

window.setup(600,600)

elsa.pencolor("white")
window.bgcolor("darkred")

root = Tk()
root.title("Keypress Control")
container_Frame = ttk.Frame(root, padding = 5)

# define our keyboard arrow key traps
def UpKeyPress(*args):
   elsa.setheading(90)
   elsa.forward(10)
   container_Frame.focus_set()

def DownKeyPress(*args):
   elsa.setheading(270)
   elsa.forward(10)
   container_Frame.focus_set()

def LeftKeyPress(*args):
   elsa.setheading(180)
   elsa.forward(10)
   container_Frame.focus_set()

def RightKeyPress(*args):
   elsa.setheading(0)
   elsa.forward(10)
   container_Frame.focus_set()

# Create our button controls
UpButton = ttk.Button(container_Frame, text = "Up", command = UpKeyPress)
DownButton = ttk.Button(container_Frame, text = "Down", command = DownKeyPress)
LeftButton = ttk.Button(container_Frame, text = "Left", command = LeftKeyPress)
RightButton = ttk.Button(container_Frame, text = "Right", command = RightKeyPress)

# Lay the controls out 
container_Frame.grid(column = 0, row = 0, sticky = (N, S, E, W))

LeftButton.grid(column = 0, row = 2, rowspan = 2, pady = 5, padx = 5)
UpButton.grid(column = 1, row = 2, pady = 5, padx = 5)
DownButton.grid(column = 1, row = 3, pady = 5, padx = 5)
RightButton.grid(column = 2, row = 2, rowspan = 2, pady = 5, padx = 5)

# bind in the key presses
container_Frame.bind('<Up>', UpKeyPress)
container_Frame.bind('<Down>', DownKeyPress)
container_Frame.bind('<Left>', LeftKeyPress)
container_Frame.bind('<Right>', RightKeyPress)
container_Frame.focus_set()

# Tell Tk to enter its event loop, which is needed to make everything run.
root.mainloop()